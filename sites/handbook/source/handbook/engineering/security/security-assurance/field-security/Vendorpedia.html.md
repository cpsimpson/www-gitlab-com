---
layout: handbook-page-toc
title: "Vendorpedia"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}


## Vendorpedia

Vendorpedia (a OneTrust tool) is an **internal only** tool designed to maintain our internal security knowledge base with a comprehensive list of questions and answers in the form of `Answer Libraries`. This will assist the Field Security team in completion of [Customer Security Assessments](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html), [RFP completion](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/Field-Security-RFP.html) and ultimately reduce the time to complete sales activities, as the tool does an automated first pass on responses. 

### RFP Completion

Vendorpedia will be used to collaborate cross-departmentally to alleviate the painpoints for the Field Team in [completion of RFPs](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/Field-Security-RFP.html). SME's from relevant departments will have access to Vendorpedia to maintain their own department specific answer libraries and be tagged in on answers that need to be added. 

### Access and Instructions
Access is currently only for Field Security team members and departmental SME's for RFP completion. 

### Maintenance and Improvements
- Quarterly, the Field Security team will conduct a quality audit of one `Answer Library` per quarter and update content as necessary. A quarter-specific label will be added (example: ~FY22Q1) indicating the last time it was audited. 
- A true up will be completed at the same time with Answerbase to maintain continuity. 

## Contact the Field Security Team
* Slack
   * Feel free to tag us with `@field-security`
   * The `#sec-fieldsecurity`, `#sec-assurance`, `#security-department` slack channels are the best place for questions relating to our team (please add the above tag)
* [GitLab field security project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/risk-field-security/)

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Field Security Homepage</a>
</div> 
