---
layout: handbook-page-toc
title: "ON24"
description: "ON24 is a sales and marketing platform for digital engagement, delivering insights to drive ​revenue growth." 
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


# About ON24

Page in progress - Recently purchased. Marketing Operations is in the process of integrating and implementing. Follow along in [epic](https://gitlab.com/groups/gitlab-com/-/epics/1800). 

## Provisioning

We have a limited number of seats. Before putting in an Access Request, please [open an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request) for Marketing Operations to review your request.   Once your request is approved, then proceed to open an [Access Request]([create one](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request)

### User Roles
ON24 licenses are only required for people who are building webinar programs (presenters/speakers do not need a license). We have a limited number of licenses and adding more comes with a price.

**Platform Login: Administrative Access** (may also be referred to as a `license` or `seat`)
- The Platform Login has full access to Elite and all functionality, including the ability to create and edit webinars
- These logins are ONLY meant for those creating, setting up, and developing webinars / events
- The ability to edit / change logins is available

**Presenter Roles: URL/Link Access**
- Presenters may log in as Producer, Presenter, or a Q&A Moderator
- All three roles will access the event via a unique link for each event

The chart below outlines the user permissions for each role (Producer, Presenter, and Q&A moderator)

|   | Producer | Presenter | Q&A Moderator |
| - | -------- | --------- | -------------|
| Upload slide and videos | :white_check_mark: |  |  |
| Add poll questions | :white_check_mark: |  |  |
| Start/stop the webinar | :white_check_mark: |  |  |
| Arrange presentation materials | :white_check_mark: |  |  |
| Change webcam layouts | :white_check_mark: |  |  |
| Mute and disconnect presenters | :white_check_mark: |  |  |
| Advance slides | :white_check_mark: | :white_check_mark: |  |
| Push poll questions to the audience | :white_check_mark: | :white_check_mark: |  |
| Whiteboarding tools | :white_check_mark: | :white_check_mark: |  |
| Screenshare | :white_check_mark: | :white_check_mark: |  |
| Q&A | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Team chat | :white_check_mark: | :white_check_mark: | :white_check_mark: |

## Resources 

Please note, you will not be able to access these trainings until you have an ON24 login.  You can request access by opening a [MOps issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request).

- [Getting Started with Webcast Elite Certification](https://training.on24.com/standard-training)
- [Preparing Your Webinar Content](https://training.on24.com/preparing-your-webinar-content)
- [Presenter Certification & Training Program](https://training.on24.com/presenter-certification-new-improved)
- [ON24 Analytics 101](https://training.on24.com/analytics-101) and [Analytics 102](https://training.on24.com/analytics-102)
- [ON24 Knowledge Center](https://on24support.force.com/Support/s/knowledge)
- [ON24 Office Hours](https://training.on24.com/office-hours-webinars?next=%2Foffice-hours-webinars%2F869178) 
